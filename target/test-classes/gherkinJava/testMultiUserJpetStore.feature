
Feature: Test_03_multiparams

	Scenario Outline: Test_03_multiparams
		Given un navigateur est ouvert
		When je suis sur la page d'accueil
		And je clique sur le lien de connexion
		And je saisi l'indentifiant utilisateur <login>
		And je saisi le mot de passe <password>
		And je clique sur le bouton login
		Then utilisateur ABC est connecte
		And je peux lire le message d'accueil <returnMessage>
    And j'attends 5 secondes
    Then je ferme le navigateur
    
    
		@profilAdmin
		Examples:
		| login | password | returnMessage |
		| "j2ee" | "j2ee" | "Welcome ABC" |

		@profilUtilisateur
		Examples:
		| login | password | returnMessage |
		| "ACID" | "ACID" | "Welcome ABC" |
