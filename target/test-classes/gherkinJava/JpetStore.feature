Feature: Connexion à l'application Jpetstore
  I want to use this template for my feature file


  Scenario: Connexion
    Given un navigateur est ouvert
    When je suis sur la page d'accueil   
    And je clique sur le lien de connexion
    And je saisi l'indentifiant utilisateur "j2ee"
    And je saisi le mot de passe "j2ee"
    And je clique sur le bouton login
    Then utilisateur ABC est connecte
    And je peux lire le message accueil "Welcom ABC!"
    And j'attends 5 secondes
    Then je ferme le navigateur
  