# language: en
Feature: Test_02_BDD

	Scenario Outline: Test_02_BDD
		Given number of cucumber <nbCucumberStart>
		When I eat <nbCucumberEtating>
		Then The rest is <nbCucumberRest>
		And I must replace <nbCucumberReplace>

		@Test01
		Examples:
		| nbCucumberEtating | nbCucumberReplace | nbCucumberRest | nbCucumberStart |
		| 4 | 6 | 6 | 10 |

		@Test02
		Examples:
		| nbCucumberEtating | nbCucumberReplace | nbCucumberRest | nbCucumberStart |
		| 1 | 1 | 1 | 2 |