/**
 * 
 */
package gherkinJava;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author Formation
 *
 */
public class JpetStoreSteps  extends CommunFeature{

	
	String login = "j2ee";
	String password = "j2ee";

	String urlSite = "https://petstore.octoperf.com/actions/Catalog.action";
	String xpathSignIn = "//a[contains(.,'Sign In')]";

	String xpathLoginInput = "//input[contains(@id,'stripes-')]";
	String xpathPassword = "//input[contains(@name,'password')]";

	String xpathValiderLogin = "//input[contains(@name,'signon')]";

	String xpathMessageAccueil = "//div[contains(@id,'WelcomeContent')]";

	String messageAccueilAttendu = "Welcome ABC!";

	WebElement lienSignIn;
	WebElement champLogin;
	WebElement champPassWord;
	WebElement boutonLogin;
	WebElement messageAccueil;

	@Given("un navigateur est ouvert")
	public void un_navigateur_est_ouvert() {
		createDriver();
	}

	@When("je suis sur la page d'accueil")
	public void je_suis_sur_la_page_d_accueil() {
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get(urlSite);
	}

	@When("je clique sur le lien de connexion")
	public void je_clique_sur_le_lien_de_connexion() {
		lienSignIn = driver.findElement(By.xpath(xpathSignIn));
		lienSignIn.click();
	}

	@When("je saisi l'indentifiant utilisateur {string}")
	public void je_saisi_l_indentifiant_utilisateur(String string) {
		champLogin = driver.findElement(By.xpath(xpathLoginInput));
		champLogin.clear();
		champLogin.sendKeys(login);
	}

	@When("je saisi le mot de passe {string}")
	public void je_saisi_le_mot_de_passe(String string) {
		champPassWord = driver.findElement(By.xpath(xpathPassword));
		champPassWord.clear();
		champPassWord.sendKeys(login);
	}

	@When("je clique sur le bouton login")
	public void je_clique_sur_le_bouton_login() {
		boutonLogin = driver.findElement(By.xpath(xpathValiderLogin));
		boutonLogin.click();
	}

	@Then("utilisateur ABC est connecte")
	public void utilisateur_abc_est_connecte() {
		messageAccueil = driver.findElement(By.xpath(xpathMessageAccueil));
		System.out.println("L'utilisateur est connecté sous ABC");
	}

//	@Then("je peux lire le message accueil {string}")
//	public void je_peux_lire_le_message_accueil(String string) {
//		
//		assertTrue("Le message d'accueil n'est pas conforme", messageAccueil.getText().equals(messageAccueilAttendu));
//		
//	}	

	@Then("j'attends {int} secondes")
	public void j_attends_secondes(Integer int1) throws InterruptedException {
	    driver.wait(int1*1000L);
	}

	@Then("je ferme le navigateur")
	public void je_ferme_le_navigateur() {
		driver.quit();
	}

}
