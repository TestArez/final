package gherkinJava;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertTrue;

public class TestRendezVous {

	WebDriver driver;

	WebElement elementEncours;
	
	
	String dateRendezVous = "21/02/2023";
	String commentaire = "Arezki";

	String xpathUserLogin;
	String urlSite = "https://katalon-demo-cura.herokuapp.com/";

	String xpathBoutonMakeAppointment = "//*[@id='btn-make-appointment']";

	String xpathUser = "//input[@id='txt-username']";

	String xpathPass = "//input[@id='txt-password']";

	String xpathBoutonLogin = "//button[@id='btn-login']";

	String nomUtilisateur = "John Doe";
	String passUtilisateur = "ThisIsNotAPassword";

	String xpathListeDeroulante = "//select[@id='combo_facility']";

	String xpathHospitalisation = " //INPUT[@id='chk_hospotal_readmission']";

	String xpathProgram = "//INPUT[@id='radio_program_medicare']";

	String xpathDate = "//INPUT[@id='txt_visit_date']";

	String xpathComment = "//TEXTAREA[@id='txt_comment']";

	String xpathBoutonBook = "//BUTTON[@id='btn-book-appointment']";

	String xpathControlFacility = "//p[@id='facility']";

	String xpathControlHospitalisation = "//P[@id='hospital_readmission']";

	String xpathControlProgram = "//P[@id='program']";

	String xpathControlDateVisit = "//P[@id='visit_date']";

	String xpathControlComment = "//P[@id='comment']";

	String xpathHomePage = "//A[@class='btn btn-default'][text()='Go to Homepage']";

	@Given("Je suis connecté sur la page de prise de rendez-vous")
	public void je_suis_connecté_sur_la_page_de_prise_de_rendez_vous() {
		createDriver();
		driver.get(urlSite);
		if (driver.findElement(By.xpath(xpathBoutonMakeAppointment)).isDisplayed()) {
			elementEncours = driver.findElement(By.xpath(xpathBoutonMakeAppointment));
			elementEncours.click();
		}
		if (driver.findElement(By.xpath(xpathUser)).isDisplayed()) {
			elementEncours = driver.findElement(By.xpath(xpathUser));
			elementEncours.click();
			elementEncours.clear();
			elementEncours.sendKeys(nomUtilisateur);
			elementEncours = driver.findElement(By.xpath(xpathPass));
			elementEncours.click();
			elementEncours.clear();
			elementEncours.sendKeys(passUtilisateur);
			elementEncours = driver.findElement(By.xpath(xpathBoutonLogin));
			elementEncours.click();
		}

	}

	@When("Je renseigne les informations obligatoires")
	public void je_renseigne_les_informations_obligatoires() {
		
		elementEncours = driver.findElement(By.xpath(xpathListeDeroulante));
		Select selection = new Select(elementEncours);
		selection.selectByIndex(1);
		
		elementEncours = driver.findElement(By.xpath(xpathHospitalisation));
		// a faire
		
		elementEncours = driver.findElement(By.xpath(xpathProgram));
		// a faire
		
		
		elementEncours = driver.findElement(By.xpath(xpathDate));
		elementEncours.click();
		elementEncours.clear();
		elementEncours.sendKeys(dateRendezVous);
		
		elementEncours = driver.findElement(By.xpath(xpathComment));
		elementEncours.click();
		elementEncours.clear();
		elementEncours.sendKeys(commentaire);
		
		
		
		
		
		
		
		
	}

	@When("Je clique sur Book Appointment")
	public void je_clique_sur_book_appointment() {
		elementEncours = driver.findElement(By.xpath(xpathBoutonBook));
		elementEncours.click();
	}

	@Then("Le rendez-vous est confirmé")
	public void le_rendez_vous_est_confirmé() {
		elementEncours = driver.findElement(By.xpath(xpathControlFacility));
		
		
		
		elementEncours = driver.findElement(By.xpath(xpathControlHospitalisation));
		
		
		elementEncours = driver.findElement(By.xpath(xpathControlProgram));
		
		
		elementEncours = driver.findElement(By.xpath(xpathControlDateVisit));
		assertTrue("Attention: la date n'est pas conforme", elementEncours.getText().equals(dateRendezVous));
		
		
		elementEncours = driver.findElement(By.xpath(xpathControlComment));		
		assertTrue("Attention: le commentaire n'est pas conforme", elementEncours.getText().equals(commentaire));
		
		elementEncours = driver.findElement(By.xpath(xpathHomePage));
		elementEncours.click();
	}

	private void createDriver() {
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
		if (null == driver) {
			this.driver = new ChromeDriver();
		}
	}
	
	

}
