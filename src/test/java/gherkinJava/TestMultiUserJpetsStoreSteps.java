package gherkinJava;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.cucumber.java.en.Then;


public class TestMultiUserJpetsStoreSteps extends CommunFeature{
	
	WebElement messageAccueil;
	String messageAccueilAttendu = "Welcome ABC!";
	String xpathMessageAccueil = "//div[contains(@id,'WelcomeContent')]";
	
	@Then("je peux lire le message accueil {string}")
	public void je_peux_lire_le_message_d_accueil(String string) {
		messageAccueil = driver.findElement(By.xpath(xpathMessageAccueil));
		assertTrue("Le message d'accueil n'est pas conforme", messageAccueil.getText().equals(messageAccueilAttendu));
	}
}
